/*======================
DISCUSSION
WD005-S3-DOM-MANIPULATiON

========================*/

//Create a container, row, and column

const container = document.createElement("div");    //create html element 'div' and put it on variable 'container'
container.setAttribute("class", 'container');       //add attribute 'class' with classname 'container'
console.log(container);         //display if element with class attribute is created

const row = document.createElement("div");
row.setAttribute("class", "row");
console.log(row);
const col = document.createElement("div");

col.setAttribute("class", "col");
console.log(col);

console.log("**************************************");
//Create a paragraph with the text "Hello, World!"
const p = document.createElement('p');
p.textContent = "Hello, Word!";


/*
Setup the elements that you've created above to display the following:
<div class="container">
    <div class="row">
        <div class="col">
            <p>Hello, World!</p>
        </div>
    </div>
</div>
*/
container.appendChild(row);
console.log(container);
row.appendChild(col);
console.log(container);
col.appendChild(p);
console.log(container);

//PUT ABOVE BEFORE THE FIRST CONTAINER
let body = document.body;
let children = body.children;
console.log(children);

body.prepend(container);


//PUT BEFORE THE LAST CONTAINER (i.e., script)
console.log(children[children.length-1]);   //access script (e.g. script is the last container)
let lastContainer = children[children.length-1];
body.insertBefore(container, lastContainer);


//PUT IN BETWEEN BATMAN AND WONDERWOMAN
let ul = document.querySelector ("ul");
let wonderWoman = ul.children[2];
ul.insertBefore(container, wonderWoman);


//CREATE A LIST ITEM WITH THE TEXT "Robin" and put it after Superman
let robin = document.createElement("li");
robin.textContent = 'Robin';
let batman = ul.children[1];
ul.insertBefore(robin, batman);


//REPLACE SUPERMAN WITH YOUR NAME
let me = document.createElement("li");
me.textContent = 'Karen';
let superman = ul.children[0];
ul.replaceChild(me, superman);


//REPLACE HELLO WORLD CONTAINER WITH JOKER
let joker = document.createElement("li");
joker.textContent = 'Joker';
ul.replaceChild(joker, container);


//ADD EVENT TO BUTTON SO THAT YOU CAN ADD A LIST ITEM INSIDE UL
// document.querySelector('#btn-print').addEventListener("click", function(){
//     alert("it's working!");
//     //console.log(document.querySelector('#input'));
//     //console.log(document.querySelector('#input').value);
//     let newHero = document.querySelector('#input').value;
//     //console.log(newHero.nodeType);  //undefined
//     //console.log(ul.appendChild(newHero)); //error (do below first so it will not result to error)
//     let li = document.createElement("li");
//     li.textContent = newHero;
//     ul.appendChild(li);
//     document.querySelector("#input").value = '';
// });



//REFACTOR YOUR CODE BY CREATING A FUNCTION CALLED createNewElement
function createNewElement(element, text){
    let newElement = document.createElement(element);
    newElement.textContent = text;
    return newElement;
}


document.querySelector("#btn-print").addEventListener("click", function(){
    let newHero = document.querySelector('#input').value;       //content from input field entered by user
    let newList = createNewElement("li", newHero);
    ul.appendChild(newList);
    document.querySelector("#input").value = '';
});


//element.setAttribute(attr, value)
function setNewAttribute(element, attr, value){
    return element.setAttribute(attr, value);
}



//LOCAL STORAGE
/*1. CREATEWHAT IS SHOWN BELOW USING JAVASCRIPT
<div class="container">
    <div class="row">
        <div class="col">
            <input type="number" id="addend1">
            <span>+<span>
            <input type="number" id="addend2">
            <button class='btn' id="btn-equals">=</button>
            <input type="number" id="sum">
            <button class='btn' id="btn-clear">CLEAR</button>
        </div>
    </div>
</div>
2. PUT ABOVE CONTAINER AND ITS CONTENT BEFORE THE SCRIPT TAG
3. WRITE A CODE TO GET NUMBERS INSIDE ADDEND1 AND ADDEND2 AND DISPLAY IT INSIDE SUM
4. USER LOCAL STORAGE TO STORE, DELETE, AND DISPLAY VALUES OF ADDEND1, ADDEND2 AND SUM
*/

//document.querySelector("#addend1").value = localStorage.getItem('')



let newContainer = createNewElement('div', '');
setNewAttribute(newContainer, 'class', 'container');

let newRow = createNewElement('div', '');
setNewAttribute(newRow, 'class', 'row');

let newCol = createNewElement('div', '');
setNewAttribute(newCol, 'class', 'col');

let input1 = createNewElement('input', '');
setNewAttribute(input1, 'type', 'number');
setNewAttribute(input1, 'id', 'addend1');

let input2 = createNewElement('input', '');
setNewAttribute(input2, 'type', 'number');
setNewAttribute(input2, 'id', 'addend2');

let input3 = createNewElement('input', '');
setNewAttribute(input3, 'type', 'number');
setNewAttribute(input3, 'id', 'sum');

let span = createNewElement('span', '+');

let button = createNewElement('button', '=');
setNewAttribute(button, 'class', 'btn');
setNewAttribute(button, 'id', 'btn-equals');

let button2 = createNewElement('button', 'CLEAR');
setNewAttribute(button2, 'class', 'btn');
setNewAttribute(button2, 'id', 'btn-clear');

console.log(newContainer);

newContainer.appendChild(newRow);
newRow.appendChild(newCol);
newCol.appendChild(input1);
newCol.appendChild(span);
newCol.appendChild(input2);
newCol.appendChild(button);
newCol.appendChild(input3);
newCol.appendChild(button2);

console.log(newContainer);

//let body = document.body;
let lastChildOfBody = body.children[body.children.length-1];
body.insertBefore(newContainer, lastChildOfBody);


//ADD (click equals on webpage)
document.querySelector("#btn-equals").addEventListener("click", function(){
    alert("hello!");
    //let addend1 = document.querySelector("#addend1").value;     //add '.value' to get the content of the input
    //let addend2 = document.querySelector("#addend2").value;     // but '.value' is string
    let addend1 = document.querySelector("#addend1").valueAsNumber;         // so used '.valueAsNumber' instead (you can see it at console when you expand the node "console.log(document.querySelectorAll('input'));")
    let addend2 = document.querySelector("#addend2").valueAsNumber;
    //console.log(addend1 + addend2);   // will simply concatinate and not add because the inputs are treated as strings
    console.log(document.querySelectorAll('input'));        //'input' is an element

    let answer = addend1 + addend2;
    sum.value = answer;
    //console.log(sum.value);

    //LOCAL STORAGE
    // see at webpage > inspect > 'Application' > Storage > Local Storage
    localStorage.setItem('addend1', addend1);
    localStorage.setItem('addend2', addend2);
    localStorage.setItem('answer', answer);

})

//CLEAR
document.querySelector("#btn-clear").addEventListener("click", function(){
    //alert("hey");
    document.querySelector("#addend1").value = "";
    document.querySelector("#addend2").value = "";
    document.querySelector("#sum").value = "";

    //LOCAL STORAGE
    //para kapag ni click ang clear, mawawala din sya sa local storage kahit irefresh ulit ang page
    localStorage.removeItem('addend1');
    localStorage.removeItem('addend2');
    localStorage.removeItem('answer');

})


//LOCAL STORAGE
// yung na-save na inputs, ilalagay/ibabalik sa loob ni input field na 'addend1' para pagka ni-refresh ang browser, makikita pa din
document.querySelector("#addend1").value = localStorage.getItem('addend1');
document.querySelector("#addend2").value = localStorage.getItem('addend2');
document.querySelector("#sum").value = localStorage.getItem('answer');


