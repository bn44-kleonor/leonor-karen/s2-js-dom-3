
/* function(e) maglalabas ng event kahit saang part ka ng webpage magclick*/
// document.addEventListener("click", function(e){
// 	//console.log(e);
// 	//console.log(e.target);
// 	//console.log(e.target.classList);
// 	//console.log(e.target.classList[1]);		/*to get 'red', get the index position of 'red'*/
// 	let color = e.target.classList[1];

// 	//console.log(document.querySelector(`#${color}-box`));		/*alamin kung saang element(or grid box) ang lalagyan ng event*/
// 	let box = document.querySelector(`#${color}-box`);
// 	box.classList.toggle(`${color}`);
// })


/* kapag kasali na si 'clear'*/
document.addEventListener("click", function(e){
	let color = e.target.classList[1];

	if(color !== 'clear'){
		let box = document.querySelector(`#${color}-box`);
		box.classList.toggle(`${color}`);
	} else {
		let boxContainer = document.querySelector("#box-container");
		let boxes = boxContainer.children;
		let boxesArr = [...boxes];			//convert boxes to Array
		let colorsArr = boxesArr.map(box => box.id.slice(0,-4));
		//console.log(colorsArr);
		for(let i = 0; i < colorsArr.length; i++){
			//element.classList.toggle(color)
			boxes[i].classList.toggle(`${colorsArr[i]}`);
		}
	}
})





