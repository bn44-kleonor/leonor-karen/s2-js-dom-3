
/*icheck kung lumalabas sya sa console, meaning nakukuha yung element btn-red*/
//console.log(document.querySelector("#btn-red"));

let redBtn = document.querySelector("#btn-red");
let redBox = document.querySelector("#red-box");
redBtn.addEventListener("click", function(){
    //alert("event happened");                            /*user alert to see if after clicking, the click is recognized*/
    //console.log(document.querySelector("#red-box"));    /*kapag ni-click ang red button, madidisplay sa condole yung element na may id na 'red-box'*/
    //redBox.style.backgroundColor = "red";
    redBox.classList.toggle("red");                       /*toggle is applicable only to 'classes' elements*/
})


let orangeBtn = document.querySelector("#btn-orange");
let orangeBox = document.querySelector("#orange-box");
orangeBtn.addEventListener("click", function(){
    //alert("event happened");                            /*user alert to see if after clicking, the click is recognized*/
    //console.log(document.querySelector("#orange-box"));    /*kapag ni-click ang red button, madidisplay sa condole yung element na may id na 'red-box'*/
    //orangeBox.style.backgroundColor = "orange";
    orangeBox.classList.toggle("orange");                       /*toggle is applicable only to 'classes' elements*/
})


let yellowBtn = document.querySelector("#btn-yellow");
let yellowBox = document.querySelector("#yellow-box");
yellowBtn.addEventListener("click", function(){
    //alert("event happened");                            /*user alert to see if after clicking, the click is recognized*/
    //console.log(document.querySelector("#yellow-box"));    /*kapag ni-click ang red button, madidisplay sa condole yung element na may id na 'red-box'*/
    //yellowBox.style.backgroundColor = "yellow";
    yellowBox.classList.toggle("yellow");                       /*toggle is applicable only to 'classes' elements*/
})


let greenBtn = document.querySelector("#btn-green");
let greenBox = document.querySelector("#green-box");
greenBtn.addEventListener("click", function(){
    //alert("event happened");                            /*user alert to see if after clicking, the click is recognized*/
    //console.log(document.querySelector("#green-box"));    /*kapag ni-click ang red button, madidisplay sa condole yung element na may id na 'red-box'*/
    //greenBox.style.backgroundColor = "green";
    greenBox.classList.toggle("green");                       /*toggle is applicable only to 'classes' elements*/
})


let blueBtn = document.querySelector("#btn-blue");
let blueBox = document.querySelector("#blue-box");
blueBtn.addEventListener("click", function(){
    //alert("event happened");                            /*user alert to see if after clicking, the click is recognized*/
    //console.log(document.querySelector("#blue-box"));    /*kapag ni-click ang red button, madidisplay sa condole yung element na may id na 'red-box'*/
    //blueBox.style.backgroundColor = "blue";
    blueBox.classList.toggle("blue");                       /*toggle is applicable only to 'classes' elements*/
})


let indigoBtn = document.querySelector("#btn-indigo");
let indigoBox = document.querySelector("#indigo-box");
indigoBtn.addEventListener("click", function(){
    //alert("event happened");                            /*user alert to see if after clicking, the click is recognized*/
    //console.log(document.querySelector("#indigo-box"));    /*kapag ni-click ang red button, madidisplay sa condole yung element na may id na 'red-box'*/
    //indigoBox.style.backgroundColor = "indigo";
    indigoBox.classList.toggle("indigo");                       /*toggle is applicable only to 'classes' elements*/
})


let clearBtn = document.querySelector("#btn-clear");
let clearBox = document.querySelector("#clear-box");

function clearBoxes(){
    //alert(event.type);
    let x = document.querySelector("#box-container");
    let color;

    //console.log(x.children[0]);
    for(let i = 0; i < x.children.length; i++) {
        //console.log(x.children[i].id);
        //console.log(x.children[i].id.slice(0,-4));  /*access the color only and exclude the '-box'*/
        color = x.children[i].id.slice(0,-4);

        if(event.type === 'click'){
            x.children[i].className = "col-4"
        } else {
            x.children[i].classList.add(color);
        }
    }
}

clearBtn.addEventListener("click", function(e){
    // redBox.style.backgroundColor = "white";
    // orangeBox.style.backgroundColor = "white";
    // yellowBox.style.backgroundColor = "white";
    // greenBox.style.backgroundColor = "white";
    // blueBox.style.backgroundColor = "white";
    // indigoBox.style.backgroundColor = "white";

    //console.log(e.type);
    clearBoxes(e);      //when you want 'e' put 'e' in function(e)
})


clearBtn.addEventListener("dblclick", function(e){
    //clearBoxes(e);      //when you want 'e' put 'e' in function(e)
    clearBoxes(e);
})
